package Gui;

import Util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by sergio on 07/12/2015.
 */
public class GestionUsuarios {

    private JPanel panel;

    private JCheckBox checkInsertar;
    private JCheckBox checkModificar;
    private JCheckBox checkEliminar;
    private JButton btGuardarCambios;

    boolean insertar;
    boolean modificar;
    boolean eliminar;

    public GestionUsuarios(){

        btGuardarCambios.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkBoxes();
                Util.MensajeDeInformacionCorrecto("Cambios guardados correctamente");
            }
        });

    }

    public void iniciarVentana(){
        JFrame frame = new JFrame("Gestión de usuarios");
        frame.getContentPane().add(panel);
        frame.pack();
        frame.setSize(300, 240);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void checkBoxes() {
        if (checkInsertar.isSelected()) {
            insertar = true;
        } else {
            insertar = false;
        }

        if (checkModificar.isSelected()) {
            modificar = true;
        } else {
            modificar = false;
        }

        if (checkEliminar.isSelected()) {
            eliminar = true;
        } else {
            eliminar = false;
        }
    }


    public boolean getInsertar() {
        return insertar;
    }

    public boolean getModificar() {
        return modificar;
    }

    public boolean getEliminar() {
        return eliminar;
    }

    public void setInsertar(boolean insertar) {
        this.insertar = insertar;
    }

    public void setModificar(boolean modificar) {
        this.modificar = modificar;
    }

    public void setEliminar(boolean eliminar) {
        this.eliminar = eliminar;
    }

}
