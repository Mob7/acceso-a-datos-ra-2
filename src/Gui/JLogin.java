package Gui;

import javax.swing.*;
import java.awt.event.*;

public class JLogin extends JDialog {
    private JPanel contentPane;
    private JButton btAceptar;
    private JButton btCancelar;
    private JTextField tfUsuario;
    private JPasswordField tfContrasena;
    private JLabel lbUsuario;
    private JLabel lbContrasena;

    private String usuario;
    private String contrasena;

    public JLogin(String titulo) {
        super();
        setContentPane(contentPane);
        setTitle(titulo);
        pack();
        setLocationRelativeTo(null);
        setModal(true);

        btAceptar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        btCancelar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        tfContrasena.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER){
                    btAceptar.doClick();
                }
            }
        });
    }

    private void onOK() {
        if (tfUsuario.getText().equals("") || tfContrasena.getText().equals("")){
            Util.Util.MensajeDeInformacion("Debes introducir usuario y contraseña");
            return;
        }

        usuario = tfUsuario.getText();
        contrasena = tfContrasena.getText();
        setVisible(false);
    }

    private void onCancel() {
        setVisible(false);
    }

    public String getUsuario() {
        return usuario;
    }

    public String getContrasena() {
        return contrasena;
    }
}
