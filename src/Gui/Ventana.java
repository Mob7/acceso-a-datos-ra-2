package Gui;

import Base.*;
import Util.Util;
import Util.AllLogins;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;

/**
 * Created by sergio on 12/11/2015.
 */
public class Ventana {
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JTextField tfArtistaNombre;
    private JTextField tfArtistaNombreOriginal;
    private JTextField tfArtistaNumeroTlfno;
    private JTextField tfArtistaPrecioActuacion;
    private JTextField tfArtistaCiudadNacimiento;
    private JTextField tfDiscoTitulo;
    private JTextField tfDiscoCopiasVendidas;
    private JTextField tfDiscoPrecioSalida;
    private JTextField tfDiscograficaNombre;
    private JTextField tfDiscograficaNumeroArtistas;
    private JTextField tfDiscograficaCiudadSede;
    private JTextField tfDiscograficaDineroRecaudado;
    private JComboBox cbDiscoCompania;
    private JRadioButton rbDiscograficaSiTributa;
    private JRadioButton rbDiscograficaNoTributa;
    private JComboBox cbDiscograficaArtistaEstrella;
    private JButton btDiscograficaModificar;
    private JButton btDiscograficaNuevo;
    private JButton btDiscograficaEliminar;
    private JButton btDiscograficaGuardar;
    private JButton btDiscoModificar;
    private JButton btDiscoNuevo;
    private JButton btDiscoGuardar;
    private JButton btDiscoEliminar;
    private JButton btArtistaModificar;
    private JButton btArtistaNuevo;
    private JButton btArtistaEliminar;
    private JButton btArtistaGuardar;
    private JLabel lbArtistaNombreArtistico;
    private JLabel lbArtistaNombreOriginal;
    private JLabel lbArtistaNumeroTelefono;
    private JLabel lbArtistaPrecioActuacion;
    private JLabel lbArtistaCiudadNacimiento;
    private JLabel lbArtistaFechaNacimiento;
    private JLabel lbDiscoTitulo;
    private JLabel lbDiscoArtista;
    private JComboBox cbDiscoArtista;
    private JLabel lbDiscoCopiasVendidas;
    private JLabel lbDiscoPrecio;
    private JLabel lbDiscoCompañia;
    private JLabel lbDiscoFechaSalida;
    private JLabel lbDiscograficaNombre;
    private JLabel lbDiscograficaCiudadSede;
    private JLabel lbDiscograficaRecaudacion;
    private JLabel lbDiscograficaTributa;
    private JLabel lbDiscograficaFechaRegistro;
    private JLabel lbDiscograficaArtistaEstrella;
    private JLabel lbDiscograficaArtista;
    private JDateChooser calDiscograficaFechaRegistro;
    private JDateChooser calArtistaFechaNacimiento;
    private JDateChooser calDiscoFechaSalida;
    private Connection conexionBase;
    private JTable tableArtistas;
    private JTable tableDiscograficas;
    private JTable tableDiscos;
    private JTextField tfDiscoBusqueda;
    private JTextField tfDiscograficaBusqueda;
    private JTextField tfArtistaBusqueda;
    private DefaultTableModel mtArtistas;
    private DefaultTableModel mtDiscograficas;
    private DefaultTableModel mtDiscos;

    private String usuario;
    private String contrasena;

    private boolean artistaModificado;
    private boolean discograficaModificada;
    private boolean discoModificado;
    private String artistaNombre;
    private String artistaNombreOriginal;
    private String discograficaNombreRegistrado;
    private String discograficaCiudadSede;
    private String discoNombre;
    private String discoArtista;
    private String titulo;

    private boolean usuarioIdentificado;
    private boolean logueadoComoAdmin;
    private boolean logueadoComoUser;
    private boolean conectadoALaBase;

    private boolean privilegioInsertar;
    private boolean privilegioModificar;
    private boolean privilegioEliminar;

    private JFrame frame = new JFrame("DiscoSTU");
    private GestionUsuarios gestion;


    private JMenuBar getMenuBar(){
        JMenuBar menuBar = new JMenuBar();

        JMenu opcionesDeConexion = new JMenu("Configuración");
        JMenuItem conexionBaseDeDatos = new JMenuItem("Conectar con phpMyAdmin (MySQL)");
        conexionBaseDeDatos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AllLogins loginPhp = new AllLogins("Conectar con phpMyAdmin");
                loginPhp.phpMyAdmin();
                conexionBase = loginPhp.getConexionBaseDeDatos();
                Util.MensajeDeInformacionCorrecto("Sesión iniciada correctamente");
                conectadoALaBase = true;
                mtArtistas.setRowCount(0);
                mtDiscograficas.setRowCount(0);
                mtDiscos.setRowCount(0);
                listarTablas(Tipo.ARTISTA);
                listarTablas(Tipo.DISCOGRAFICA);
                listarTablas(Tipo.DISCO);
                anadirCombos();
                actualizarMenuBar();
            }
        });
        JMenuItem desconexionBaseDeDatos = new JMenuItem("Desconectar");
        desconexionBaseDeDatos.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    conexionBase.close();
                    Util.MensajeDeInformacionCorrecto("Conexión con la base de datos cerrada correctamente");
                    conectadoALaBase = false;
                    mtArtistas.setRowCount(0);
                    mtDiscograficas.setRowCount(0);
                    mtDiscos.setRowCount(0);
                    actualizarMenuBar();
                } catch (SQLException sqle){
                    sqle.printStackTrace();
                    Util.MensajeDeInformacion("Error al desconectar");
                }

            }
        });
        JMenuItem gestionDeUsuarios = new JMenuItem("Gestionar usuarios");
        gestionDeUsuarios.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gestion = new GestionUsuarios();
                gestion.iniciarVentana();
            }
        });
        JMenuItem cambiarDeUsuario = new JMenuItem("Cambiar de usuario");
        cambiarDeUsuario.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logueadoComoAdmin=false;
                logueadoComoUser=false;
                loguearUsuario();
                cogerPermisos();
                establecerPermisos();
                actualizarMenuBar();
            }
        });

        if (conectadoALaBase){
            opcionesDeConexion.add(desconexionBaseDeDatos);
        } else {
            opcionesDeConexion.add(conexionBaseDeDatos);
        }
        if (logueadoComoAdmin && conectadoALaBase){
            opcionesDeConexion.add(gestionDeUsuarios);
        }
        if (conectadoALaBase){
            opcionesDeConexion.add(cambiarDeUsuario);
        }

        menuBar.add(opcionesDeConexion);

        return menuBar;
    }

    private enum Tipo {
        ARTISTA, DISCO, DISCOGRAFICA
    }

    public Ventana() {

        conectadoALaBase = false;
        usuarioIdentificado=false;
        logueadoComoAdmin=false;
        logueadoComoUser=false;

        gestion = new GestionUsuarios();

        gestion.setInsertar(true);
        gestion.setModificar(false);
        gestion.setEliminar(false);


        iniciarVentana();
        iniciarTablas(Tipo.ARTISTA);
        iniciarTablas(Tipo.DISCOGRAFICA);
        iniciarTablas(Tipo.DISCO);

        try {
            loguearUsuario();
            titulo = "Inicie sesión en phpMyAdmin";
            conectar();
            Util.MensajeDeInformacionCorrecto("Sesión iniciada correctamente");
            conectadoALaBase = true;
            actualizarMenuBar();
            listarTablas(Tipo.ARTISTA);
            listarTablas(Tipo.DISCOGRAFICA);
            listarTablas(Tipo.DISCO);
            anadirCombos();
        } catch (ClassNotFoundException cnfe) {
            JOptionPane.showMessageDialog(null,
                    "No se ha podido cargar el driver del SGBD",
                    "Conectar", JOptionPane.ERROR_MESSAGE);
        } catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null,
                    "No se ha podido conectar con el servidor. Comprueba que está arrancado",
                    "Conectar", JOptionPane.ERROR_MESSAGE);
        }


        artistaModificado = false;
        discograficaModificada = false;
        discoModificado = false;

        tableArtistas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int indexFila = tableArtistas.getSelectedRow();
                int indexColumna = tableArtistas.getSelectedColumn();
                if (indexColumna > -1) {
                    cargarDatosSeleccionados(indexFila, Tipo.ARTISTA);
                }
                artistaModificado = false;
                btArtistaModificar.setEnabled(true);
                btArtistaEliminar.setEnabled(true);
                if (logueadoComoUser==true && privilegioModificar==false){
                    btArtistaModificar.setEnabled(false);
                }
                if (logueadoComoUser==true && privilegioEliminar==false){
                    btArtistaEliminar.setEnabled(false);
                }
            }


            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        tableDiscograficas.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int indexFila = tableDiscograficas.getSelectedRow();
                int indexColumna = tableDiscograficas.getSelectedColumn();
                if (indexColumna > -1) {
                    cargarDatosSeleccionados(indexFila, Tipo.DISCOGRAFICA);
                }
                discograficaModificada = false;
                btDiscograficaModificar.setEnabled(true);
                btDiscograficaEliminar.setEnabled(true);
                if (logueadoComoUser==true && privilegioModificar==false){
                    btDiscograficaModificar.setEnabled(false);
                }
                if (logueadoComoUser==true && privilegioEliminar==false){
                    btDiscograficaEliminar.setEnabled(false);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        tableDiscos.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int indexFila = tableDiscos.getSelectedRow();
                int indexColumna = tableDiscos.getSelectedColumn();
                if (indexColumna > -1) {
                    cargarDatosSeleccionados(indexFila, Tipo.DISCO);
                }
                discoModificado = false;
                btDiscoModificar.setEnabled(true);
                btDiscoEliminar.setEnabled(true);
                if (logueadoComoUser==true && privilegioModificar==false){
                    btDiscoModificar.setEnabled(false);
                }
                if (logueadoComoUser==true && privilegioEliminar==false){
                    btDiscoEliminar.setEnabled(false);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        btArtistaNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limpiarCajas(Tipo.ARTISTA);
                activarCajas(Tipo.ARTISTA, true);
                seleccionarEditable(Tipo.ARTISTA, true);


            }
        });

        btDiscograficaNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limpiarCajas(Tipo.DISCOGRAFICA);
                activarCajas(Tipo.DISCOGRAFICA, true);
                seleccionarEditable(Tipo.DISCOGRAFICA, true);
            }
        });

        btDiscoNuevo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                limpiarCajas(Tipo.DISCO);
                activarCajas(Tipo.DISCO, true);
                seleccionarEditable(Tipo.DISCO, true);
            }
        });

        btArtistaGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDatos(Tipo.ARTISTA, artistaModificado);
            }
        });

        btDiscograficaGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDatos(Tipo.DISCOGRAFICA, discograficaModificada);
            }
        });

        btDiscoGuardar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guardarDatos(Tipo.DISCO, discoModificado);
            }
        });

        btArtistaModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificarDatos(Tipo.ARTISTA);
            }
        });

        btDiscograficaModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificarDatos(Tipo.DISCOGRAFICA);
            }
        });

        btDiscoModificar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                modificarDatos(Tipo.DISCO);
            }
        });

        btArtistaEliminar.addActionListener(new ActionListener() {
            int indexFila = tableArtistas.getSelectedRow();
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarDatos(Tipo.ARTISTA, artistaModificado);
            }
        });

        btDiscograficaEliminar.addActionListener(new ActionListener() {
            int indexFila = tableDiscograficas.getSelectedRow();
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarDatos(Tipo.DISCOGRAFICA, discograficaModificada);
            }
        });

        btDiscoEliminar.addActionListener(new ActionListener() {
            int indexFila = tableDiscos.getSelectedRow();
            @Override
            public void actionPerformed(ActionEvent e) {
                eliminarDatos(Tipo.DISCO, discoModificado);
            }
        });

        tfArtistaBusqueda.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                buscar(Tipo.ARTISTA, tfArtistaBusqueda.getText());
            }
        });

        tfDiscograficaBusqueda.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                buscar(Tipo.DISCOGRAFICA, tfDiscograficaBusqueda.getText());
            }
        });

        tfDiscoBusqueda.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                buscar(Tipo.DISCO, tfDiscoBusqueda.getText());
            }
        });

    }

    private void iniciarVentana(){
        frame.setJMenuBar(getMenuBar());
        getMenuBar().setVisible(true);
        frame.getContentPane().add(panel1);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    private void conectar() throws ClassNotFoundException,
            SQLException {

        JLogin login = new JLogin(titulo);
        login.setVisible(true);

        usuario = login.getUsuario();
        contrasena = login.getContrasena();

        Class.forName("com.mysql.jdbc.Driver");
        conexionBase = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/discostu", usuario, contrasena);
    }

    private void anadirCombos(){
        cbDiscograficaArtistaEstrella.removeAllItems();
        try
        {
            Statement sentence = conexionBase.createStatement();

            ResultSet rs= sentence.executeQuery("select nombre_artistico from Artistas");
            while(rs.next()){
                cbDiscograficaArtistaEstrella.addItem(rs.getString(1));
            }
        }
        catch(Exception e)
        {
            System.out.println("Error"+e);
        }

        cbDiscoArtista.removeAllItems();
        try
        {
            Statement sentence = conexionBase.createStatement();

            ResultSet rs= sentence.executeQuery("select nombre_artistico from Artistas");
            while(rs.next()){
                cbDiscoArtista.addItem(rs.getString(1));
            }

        }
        catch(Exception e)
        {
            System.out.println("Error"+e);
        }

        cbDiscoCompania.removeAllItems();
        try
        {
            Statement sentence = conexionBase.createStatement();

            ResultSet rs2= sentence.executeQuery("select nombre_registrado from Discograficas");
            while(rs2.next()){
                cbDiscoCompania.addItem(rs2.getString(1));
            }
        }
        catch(Exception e)
        {
            System.out.println("Error"+e);
        }

    }

    private void iniciarTablas(Tipo tipo){

        switch (tipo){
            case ARTISTA:
                mtArtistas = new DefaultTableModel();
                mtArtistas.addColumn("Nombre artístico");
                mtArtistas.addColumn("Nombre original");
                mtArtistas.addColumn("Nº teléfono");
                mtArtistas.addColumn("Precio por actuación");
                mtArtistas.addColumn("Ciudad de nacimiento");
                mtArtistas.addColumn("Fecha de nacimiento");

                tableArtistas.setModel(mtArtistas);
                break;

            case DISCOGRAFICA:
                mtDiscograficas = new DefaultTableModel();
                mtDiscograficas.addColumn("Nombre registrado");
                mtDiscograficas.addColumn("Artistas");
                mtDiscograficas.addColumn("Ciudad de sede");
                mtDiscograficas.addColumn("Dinero recaudado");
                mtDiscograficas.addColumn("Tributa en España");
                mtDiscograficas.addColumn("Artista estrella");
                mtDiscograficas.addColumn("Fecha de registro");

                tableDiscograficas.setModel(mtDiscograficas);
                break;

            case DISCO:
                mtDiscos = new DefaultTableModel();
                mtDiscos.addColumn("Título");
                mtDiscos.addColumn("Artista");
                mtDiscos.addColumn("Copias vendidas");
                mtDiscos.addColumn("Precio");
                mtDiscos.addColumn("Compañía discográfica");
                mtDiscos.addColumn("Fecha de salida");

                tableDiscos.setModel(mtDiscos);
                break;
        }

    }

    private void listarTablas(Tipo tipo){

        String sql;

        switch (tipo){

            case ARTISTA:
                sql = "SELECT * FROM Artistas";
                try {
                    PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                    ResultSet resultado = sentencia.executeQuery();

                    while (resultado.next()) {
                        int id = resultado.getInt("id");
                        String nombre_artistico = resultado.getString("nombre_artistico");
                        String nombre_original = resultado.getString("nombre_original");
                        int numero_telefono = resultado.getInt("numero_telefono");
                        Float precio_actuacion = resultado.getFloat("precio_actuacion");
                        String ciudad_nacimiento = resultado.getString("ciudad_nacimiento");
                        Date fecha_nacimiento = resultado.getDate("fecha_nacimiento");

                        Object[] fila = new Object[]{nombre_artistico, nombre_original, numero_telefono, precio_actuacion, ciudad_nacimiento, fecha_nacimiento};

                        mtArtistas.addRow(fila);
                    }
                } catch (SQLException sqle) {
                    Util.MensajeDeInformacion("Error al listar los artistas");
                    sqle.printStackTrace();
                }

                break;

            case DISCOGRAFICA:
                sql = "SELECT * FROM Discograficas";
                try {
                    PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                    ResultSet resultado = sentencia.executeQuery();

                    while (resultado.next()) {
                        int id = resultado.getInt("id");
                        String nombre_registrado = resultado.getString("nombre_registrado");
                        int numero_artistas_contratados = resultado.getInt("numero_artistas_contratados");
                        String ciudad_sede = resultado.getString("ciudad_sede");
                        Float dinero_recaudado = resultado.getFloat("dinero_recaudado");
                        Boolean tributa = resultado.getBoolean("tributa");
                        String artista_estrella = resultado.getString("artista_estrella");
                        Date fecha_registro_empresarial = resultado.getDate("fecha_registro_empresarial");

                        Object[] fila = new Object[]{nombre_registrado, numero_artistas_contratados, ciudad_sede, dinero_recaudado, tributa, artista_estrella, fecha_registro_empresarial};

                        mtDiscograficas.addRow(fila);
                    }
                } catch (SQLException sqle) {
                    Util.MensajeDeInformacion("Error al listar las discográficas");
                    sqle.printStackTrace();
                }
                break;

            case DISCO:
                sql = "SELECT * FROM Discos";
                try {
                    PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                    ResultSet resultado = sentencia.executeQuery();

                    while (resultado.next()) {
                        int id = resultado.getInt("id");
                        String titulo = resultado.getString("titulo");
                        String artista = resultado.getString("artista");
                        int copias_vendidas = resultado.getInt("copias_vendidas");
                        Float precio_salida = resultado.getFloat("precio_salida");
                        String compania_discografica = resultado.getString("compania_discografica");
                        Date fecha_salida = resultado.getDate("fecha_salida");

                        Object[] fila = new Object[]{titulo, artista, copias_vendidas, precio_salida, compania_discografica, fecha_salida};

                        mtDiscos.addRow(fila);
                    }
                } catch (SQLException sqle) {
                    Util.MensajeDeInformacion("Error al listar las discográficas");
                    sqle.printStackTrace();
                }
                break;

        }
    }

    private void cargarDatosSeleccionados(int indiceFila, Tipo tipo){
        switch (tipo){

            case ARTISTA:

                btArtistaGuardar.setEnabled(false);

                tfArtistaNombre.setText(tableArtistas.getValueAt(indiceFila, 0).toString());
                tfArtistaNombreOriginal.setText(tableArtistas.getValueAt(indiceFila, 1).toString());
                tfArtistaNumeroTlfno.setText(tableArtistas.getValueAt(indiceFila, 2).toString());
                tfArtistaPrecioActuacion.setText(tableArtistas.getValueAt(indiceFila, 3).toString());
                tfArtistaCiudadNacimiento.setText(tableArtistas.getValueAt(indiceFila, 4).toString());
                calArtistaFechaNacimiento.setDate(Date.valueOf(tableArtistas.getValueAt(indiceFila, 5).toString()));

                seleccionarEditable(Tipo.ARTISTA, false);

                break;

            case DISCOGRAFICA:

                btDiscograficaGuardar.setEnabled(false);

                tfDiscograficaNombre.setText(tableDiscograficas.getValueAt(indiceFila, 0).toString());
                tfDiscograficaNumeroArtistas.setText(tableDiscograficas.getValueAt(indiceFila, 1).toString());
                tfDiscograficaCiudadSede.setText(tableDiscograficas.getValueAt(indiceFila, 2).toString());
                tfDiscograficaDineroRecaudado.setText(tableDiscograficas.getValueAt(indiceFila, 3).toString());
                if (tableDiscograficas.getValueAt(indiceFila, 4).toString().equals("true")){
                    rbDiscograficaSiTributa.setSelected(true);
                    rbDiscograficaNoTributa.setSelected(false);
                } else {
                    rbDiscograficaSiTributa.setSelected(false);
                    rbDiscograficaNoTributa.setSelected(true);
                }
                cbDiscograficaArtistaEstrella.setSelectedItem(tableDiscograficas.getValueAt(indiceFila, 5).toString());
                calDiscograficaFechaRegistro.setDate(Date.valueOf(tableDiscograficas.getValueAt(indiceFila, 6).toString()));

                seleccionarEditable(Tipo.DISCOGRAFICA, false);

                break;

            case DISCO:

                btDiscoGuardar.setEnabled(false);

                tfDiscoTitulo.setText(tableDiscos.getValueAt(indiceFila, 0).toString());
                cbDiscoArtista.setSelectedItem(tableDiscos.getValueAt(indiceFila, 1).toString());
                tfDiscoCopiasVendidas.setText(tableDiscos.getValueAt(indiceFila, 2).toString());
                tfDiscoPrecioSalida.setText(tableDiscos.getValueAt(indiceFila, 3).toString());
                cbDiscoCompania.setSelectedItem(tableDiscos.getValueAt(indiceFila, 4).toString());
                calDiscoFechaSalida.setDate(Date.valueOf(tableDiscos.getValueAt(indiceFila, 5).toString()));

                seleccionarEditable(Tipo.DISCO, false);

                break;
        }

    }

    private void seleccionarEditable(Tipo tipo, boolean Sino){
        switch (tipo){

            case ARTISTA:
                tfArtistaNombre.setEditable(Sino);
                tfArtistaNombreOriginal.setEditable(Sino);
                tfArtistaNumeroTlfno.setEditable(Sino);
                tfArtistaPrecioActuacion.setEditable(Sino);
                tfArtistaCiudadNacimiento.setEditable(Sino);
                calArtistaFechaNacimiento.setEnabled(Sino);

                tfArtistaNombre.setEnabled(Sino);
                tfArtistaNombreOriginal.setEnabled(Sino);
                tfArtistaNumeroTlfno.setEnabled(Sino);
                tfArtistaPrecioActuacion.setEnabled(Sino);
                tfArtistaCiudadNacimiento.setEnabled(Sino);
                calArtistaFechaNacimiento.setEnabled(Sino);

                break;

            case DISCOGRAFICA:
                tfDiscograficaNombre.setEditable(Sino);
                tfDiscograficaNumeroArtistas.setEditable(Sino);
                tfDiscograficaCiudadSede.setEditable(Sino);
                tfDiscograficaDineroRecaudado.setEditable(Sino);
                rbDiscograficaSiTributa.setEnabled(Sino);
                rbDiscograficaNoTributa.setEnabled(Sino);
                cbDiscograficaArtistaEstrella.setEditable(Sino);
                calDiscograficaFechaRegistro.setEnabled(Sino);

                tfDiscograficaNombre.setEnabled(Sino);
                tfDiscograficaNumeroArtistas.setEnabled(Sino);
                tfDiscograficaCiudadSede.setEnabled(Sino);
                tfDiscograficaDineroRecaudado.setEnabled(Sino);
                rbDiscograficaSiTributa.setEnabled(Sino);
                rbDiscograficaNoTributa.setEnabled(Sino);
                cbDiscograficaArtistaEstrella.setEnabled(Sino);
                calDiscograficaFechaRegistro.setEnabled(Sino);

                break;

            case DISCO:
                tfDiscoTitulo.setEditable(Sino);
                cbDiscoArtista.setEditable(Sino);
                tfDiscoCopiasVendidas.setEditable(Sino);
                tfDiscoPrecioSalida.setEditable(Sino);
                cbDiscoCompania.setEditable(Sino);
                calDiscoFechaSalida.setEnabled(Sino);

                tfDiscoTitulo.setEnabled(Sino);
                cbDiscoArtista.setEnabled(Sino);
                tfDiscoCopiasVendidas.setEnabled(Sino);
                tfDiscoPrecioSalida.setEnabled(Sino);
                cbDiscoCompania.setEnabled(Sino);
                calDiscoFechaSalida.setEnabled(Sino);

                break;
        }

    }

    private void limpiarCajas(Tipo tipo){
        switch (tipo){

            case ARTISTA:
                tfArtistaNombre.setText("");
                tfArtistaNombreOriginal.setText("");
                tfArtistaNumeroTlfno.setText("");
                tfArtistaPrecioActuacion.setText("");
                tfArtistaCiudadNacimiento.setText("");
                calArtistaFechaNacimiento.setDate(null);

                artistaModificado = false;

                break;

            case DISCOGRAFICA:
                    tfDiscograficaNombre.setText("");
                    tfDiscograficaNumeroArtistas.setText("");
                    tfDiscograficaCiudadSede.setText("");
                    tfDiscograficaDineroRecaudado.setText("");
                    rbDiscograficaSiTributa.setSelected(false);
                    rbDiscograficaNoTributa.setSelected(false);
                    calDiscograficaFechaRegistro.setDate(null);

                discograficaModificada = false;

                break;

            case DISCO:
                    tfDiscoTitulo.setText("");
                    tfDiscoCopiasVendidas.setText("");
                    tfDiscoPrecioSalida.setText("");
                    calDiscoFechaSalida.setDate(null);

                discoModificado = false;

                break;
        }


    }

    private void activarCajas(Tipo tipo, boolean nuevo){
        switch (tipo){

            case ARTISTA:

                tfArtistaNombre.setEnabled(nuevo);
                tfArtistaNombreOriginal.setEnabled(nuevo);
                tfArtistaNumeroTlfno.setEnabled(nuevo);
                tfArtistaPrecioActuacion.setEnabled(nuevo);
                tfArtistaCiudadNacimiento.setEnabled(nuevo);
                calArtistaFechaNacimiento.setEnabled(nuevo);

                btArtistaNuevo.setEnabled(nuevo);
                btArtistaModificar.setEnabled(!nuevo);
                btArtistaEliminar.setEnabled(!nuevo);
                btArtistaGuardar.setEnabled(nuevo);

                break;

            case DISCOGRAFICA:

                tfDiscograficaNombre.setEnabled(nuevo);
                tfDiscograficaNumeroArtistas.setEnabled(nuevo);
                tfDiscograficaCiudadSede.setEnabled(nuevo);
                tfDiscograficaDineroRecaudado.setEnabled(nuevo);
                rbDiscograficaSiTributa.setEnabled(nuevo);
                rbDiscograficaNoTributa.setEnabled(nuevo);
                cbDiscograficaArtistaEstrella.setEnabled(nuevo);
                calDiscograficaFechaRegistro.setEnabled(nuevo);

                btDiscograficaNuevo.setEnabled(nuevo);
                btDiscograficaModificar.setEnabled(!nuevo);
                btDiscograficaEliminar.setEnabled(!nuevo);
                btDiscograficaGuardar.setEnabled(nuevo);

                seleccionarEditable(Tipo.DISCOGRAFICA, true);

                break;

            case DISCO:

                tfDiscoTitulo.setEnabled(nuevo);
                cbDiscoArtista.setEnabled(nuevo);
                tfDiscoCopiasVendidas.setEnabled(nuevo);
                tfDiscoPrecioSalida.setEnabled(nuevo);
                cbDiscoCompania.setEnabled(nuevo);
                calDiscoFechaSalida.setEnabled(nuevo);

                btDiscoNuevo.setEnabled(nuevo);
                btDiscoModificar.setEnabled(!nuevo);
                btDiscoEliminar.setEnabled(!nuevo);
                btDiscoGuardar.setEnabled(nuevo);

                seleccionarEditable(Tipo.DISCO, true);

                break;

        }
    }

    private void modificarDatos(Tipo tipo){

        switch (tipo){

            case ARTISTA:
                seleccionarEditable(Tipo.ARTISTA, true);
                artistaNombre = tfArtistaNombre.getText();
                artistaNombreOriginal = tfArtistaNombreOriginal.getText();

                artistaModificado = true;
                btArtistaGuardar.setEnabled(true);

                break;

            case DISCOGRAFICA:
                seleccionarEditable(Tipo.DISCOGRAFICA, true);
                discograficaNombreRegistrado = tfDiscograficaNombre.getText();
                discograficaCiudadSede = tfDiscograficaCiudadSede.getText();

                discograficaModificada = true;
                btDiscograficaGuardar.setEnabled(true);

                break;

            case DISCO:
                seleccionarEditable(Tipo.DISCO, true);
                discoNombre = tfDiscoTitulo.getText();
                discoArtista = cbDiscoArtista.getSelectedItem().toString();

                discoModificado = true;
                btDiscoGuardar.setEnabled(true);

                break;
        }
    }

    private void guardarDatos(Tipo tipo, Boolean nuevo){

        switch (tipo){
            case ARTISTA:
                if (nuevo == true){
                    if (!tfArtistaNombre.getText().equals("") && !tfArtistaNombreOriginal.getText().equals("") && !tfArtistaNumeroTlfno.getText().equals("")
                            && !tfArtistaPrecioActuacion.getText().equals("") && !tfArtistaCiudadNacimiento.getText().equals("") && !calArtistaFechaNacimiento.getDate().equals(null)){
                        eliminarDatos(Tipo.ARTISTA, true);
                        activarCajasTrasEliminar(Tipo.ARTISTA, false);
                        mtArtistas.setRowCount(0);
                        listarTablas(Tipo.ARTISTA);
                        anadirCombos();
                        Util.MensajeDeAlerta("Artista modificado correctamente");

                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }
                } else {
                    if (!tfArtistaNombre.getText().equals("") && !tfArtistaNombreOriginal.getText().equals("") && !tfArtistaNumeroTlfno.getText().equals("")
                            && !tfArtistaPrecioActuacion.getText().equals("") && !tfArtistaCiudadNacimiento.getText().equals("") && !calArtistaFechaNacimiento.getDate().equals(null)){

                        String sql = "INSERT INTO Artistas (nombre_artistico, nombre_original, numero_telefono, precio_actuacion, ciudad_nacimiento, fecha_nacimiento) "+"VALUES (?, ?, ?, ?, ?, ?)";

                        try {
                            PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                            sentencia.setString(1, tfArtistaNombre.getText());
                            sentencia.setString(2, tfArtistaNombreOriginal.getText());
                            sentencia.setInt(3, Integer.parseInt(tfArtistaNumeroTlfno.getText()));
                            sentencia.setFloat(4, Float.parseFloat(tfArtistaPrecioActuacion.getText()));
                            sentencia.setString(5, tfArtistaCiudadNacimiento.getText());
                            sentencia.setDate(6, new Date(calArtistaFechaNacimiento.getDate().getTime()));

                            sentencia.executeUpdate();
                        } catch (SQLException sqle) {
                            Util.MensajeDeAlerta("Error al dar de alta");
                        }
                        limpiarCajas(Tipo.ARTISTA);
                        mtArtistas.setRowCount(0);
                        listarTablas(Tipo.ARTISTA);
                        anadirCombos();
                        Util.MensajeDeAlerta("Artista añadido a la lista");
                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }
                }


                break;

            case DISCOGRAFICA:
                boolean tributa;
                String artistaEstrellaSeleccionado = cbDiscograficaArtistaEstrella.getSelectedItem().toString();

                if (nuevo == true){
                    if (!tfDiscograficaNombre.getText().equals("") && !tfDiscograficaNumeroArtistas.getText().equals("") && !tfDiscograficaCiudadSede.getText().equals("")
                            && !tfDiscograficaDineroRecaudado.getText().equals("") && artistaEstrellaSeleccionado!=null && !calDiscograficaFechaRegistro.getDate().equals(null)){
                        eliminarDatos(Tipo.DISCOGRAFICA, true);
                        activarCajasTrasEliminar(Tipo.DISCOGRAFICA, false);
                        mtDiscograficas.setRowCount(0);
                        listarTablas(Tipo.DISCOGRAFICA);
                        anadirCombos();
                        Util.MensajeDeAlerta("Discográfica modificada correctamente");

                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }
                } else {
                    if (!tfDiscograficaNombre.getText().equals("") && !tfDiscograficaNumeroArtistas.getText().equals("") && !tfDiscograficaCiudadSede.getText().equals("")
                            && !tfDiscograficaDineroRecaudado.getText().equals("") && artistaEstrellaSeleccionado!=null && !calDiscograficaFechaRegistro.getDate().equals(null)){
                        Tributa tributos = null;

                        if (rbDiscograficaSiTributa.isSelected()){
                            tributa = true;
                        } else {
                            tributa = false;
                        }

                        String sql = "INSERT INTO Discograficas (nombre_registrado, numero_artistas_contratados, ciudad_sede, dinero_recaudado, tributa, artista_estrella, fecha_registro_empresarial) "+"VALUES (?, ?, ?, ?, ?, ?, ?)";

                        try {
                            PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                            sentencia.setString(1, tfDiscograficaNombre.getText());
                            sentencia.setInt(2, Integer.parseInt(tfDiscograficaNumeroArtistas.getText()));
                            sentencia.setString(3, tfDiscograficaCiudadSede.getText());
                            sentencia.setFloat(4, Float.parseFloat(tfDiscograficaDineroRecaudado.getText()));
                            sentencia.setBoolean(5, tributa);
                            sentencia.setString(6, artistaEstrellaSeleccionado);
                            sentencia.setDate(7, new Date(calDiscograficaFechaRegistro.getDate().getTime()));

                            sentencia.executeUpdate();
                        } catch (SQLException sqle) {
                            sqle.printStackTrace();
                            Util.MensajeDeAlerta("Error al dar de alta");
                        }
                        limpiarCajas(Tipo.DISCOGRAFICA);
                        mtDiscograficas.setRowCount(0);
                        listarTablas(Tipo.DISCOGRAFICA);
                        anadirCombos();
                        Util.MensajeDeAlerta("Discográfica añadida a la lista");
                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }

                }

                break;

            case DISCO:

                String artistaDelDisco = cbDiscoArtista.getSelectedItem().toString();
                String discograficaDelDisco = cbDiscoCompania.getSelectedItem().toString();

                if (nuevo == true){
                    if (!tfDiscoTitulo.getText().equals("") && artistaDelDisco!=null && !tfDiscoCopiasVendidas.getText().equals("") && !tfDiscoPrecioSalida.getText().equals("")
                            && discograficaDelDisco!=null && !calDiscoFechaSalida.getDate().equals(null)){
                        eliminarDatos(Tipo.DISCO, true);
                        activarCajasTrasEliminar(Tipo.DISCO, false);
                        mtDiscos.setRowCount(0);
                        listarTablas(Tipo.DISCO);
                        anadirCombos();
                        Util.MensajeDeAlerta("Disco modificado correctamente");

                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }

                } else {
                    if (!tfDiscoTitulo.getText().equals("") && artistaDelDisco!=null && !tfDiscoCopiasVendidas.getText().equals("") && !tfDiscoPrecioSalida.getText().equals("")
                            && discograficaDelDisco!=null && !calDiscoFechaSalida.getDate().equals(null)){

                        String sql = "INSERT INTO Discos (titulo, artista, copias_vendidas, precio_salida, compania_discografica, fecha_salida) "+"VALUES (?, ?, ?, ?, ?, ?)";

                        try {
                            PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                            sentencia.setString(1, tfDiscoTitulo.getText());
                            sentencia.setString(2, artistaDelDisco);
                            sentencia.setInt(3, Integer.parseInt(tfDiscoCopiasVendidas.getText()));
                            sentencia.setFloat(4, Float.parseFloat(tfDiscoPrecioSalida.getText()));
                            sentencia.setString(5, discograficaDelDisco);
                            sentencia.setDate(6, new Date(calDiscoFechaSalida.getDate().getTime()));

                            sentencia.executeUpdate();
                        } catch (SQLException sqle) {
                            sqle.printStackTrace();
                            Util.MensajeDeAlerta("Error al dar de alta");
                        }
                        limpiarCajas(Tipo.DISCO);
                        mtDiscos.setRowCount(0);
                        listarTablas(Tipo.DISCO);
                        anadirCombos();
                        Util.MensajeDeAlerta("Disco añadido a la lista");
                    } else {
                        Util.MensajeDeAlerta("Error. Es necesario rellenar todos los datos");
                    }
                }

                break;
        }

    }

    private void eliminarDatos(Tipo tipo, boolean modificado){
        String sql;

        switch (tipo){

            case ARTISTA:

                if (modificado == true){
                    try {

                        sql = "UPDATE Artistas SET nombre_artistico = ?, nombre_original = ?, numero_telefono = ?, precio_actuacion = ?, ciudad_nacimiento = ?, fecha_nacimiento = ? WHERE nombre_artistico = ?;";

                        PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                        sentencia.setString(1, tfArtistaNombre.getText());
                        sentencia.setString(2, tfArtistaNombreOriginal.getText());
                        sentencia.setInt(3, Integer.parseInt(tfArtistaNumeroTlfno.getText()));
                        sentencia.setFloat(4, Float.parseFloat(tfArtistaPrecioActuacion.getText()));
                        sentencia.setString(5, tfArtistaCiudadNacimiento.getText());
                        sentencia.setDate(6, new Date(calArtistaFechaNacimiento.getDate().getTime()));
                        sentencia.setString(7, artistaNombre);

                        sentencia.executeUpdate();
                    } catch (SQLException sqle) {
                        Util.MensajeDeAlerta("Error al modificar");
                    }
                } else {

                    try {

                        sql = "DELETE FROM Artistas WHERE nombre_artistico = ? and nombre_original = ?;";

                        PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                        sentencia.setString(1, tfArtistaNombre.getText());
                        sentencia.setString(2, tfArtistaNombreOriginal.getText());


                        sentencia.executeUpdate();

                    } catch (SQLException sqle) {
                        Util.MensajeDeAlerta("Error al eliminar");
                    }
                    activarCajasTrasEliminar(Tipo.ARTISTA, false);
                    mtArtistas.setRowCount(0);
                    listarTablas(Tipo.ARTISTA);
                    anadirCombos();
                    Util.MensajeDeInformacion("Artista eliminado correctamente");

                }

                break;

            case DISCOGRAFICA:

                if (modificado == true){

                    boolean tributa;
                    String artistaEstrellaSeleccionado = cbDiscograficaArtistaEstrella.getSelectedItem().toString();
                    if (rbDiscograficaSiTributa.isSelected()){
                        tributa = true;
                    } else {
                        tributa = false;
                    }

                    try {

                        sql = "UPDATE Discograficas SET nombre_registrado = ?, numero_artistas_contratados = ?, ciudad_sede = ?, dinero_recaudado = ?, tributa = ?, artista_estrella = ?, fecha_registro_empresarial = ? WHERE nombre_registrado = ? AND ciudad_sede = ?;";

                        PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                        sentencia.setString(1, tfDiscograficaNombre.getText());
                        sentencia.setInt(2, Integer.parseInt(tfDiscograficaNumeroArtistas.getText()));
                        sentencia.setString(3, tfDiscograficaCiudadSede.getText());
                        sentencia.setFloat(4, Float.parseFloat(tfDiscograficaDineroRecaudado.getText()));
                        sentencia.setBoolean(5, tributa);
                        sentencia.setString(6, artistaEstrellaSeleccionado);
                        sentencia.setDate(7, new Date(calDiscograficaFechaRegistro.getDate().getTime()));
                        sentencia.setString(8, discograficaNombreRegistrado);
                        sentencia.setString(9, discograficaCiudadSede);

                        sentencia.executeUpdate();
                    } catch (SQLException sqle) {
                        Util.MensajeDeAlerta("Error al modificar");
                    }

                } else {

                    try {

                        sql = "DELETE FROM Discograficas WHERE nombre_registrado = ? and ciudad_sede = ?;";


                        PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                        sentencia.setString(1, tfDiscograficaNombre.getText());
                        sentencia.setString(2, tfDiscograficaCiudadSede.getText());

                        sentencia.executeUpdate();

                    } catch (SQLException sqle) {
                        Util.MensajeDeAlerta("Error al eliminar");
                    }
                    activarCajasTrasEliminar(Tipo.DISCOGRAFICA, false);
                    mtDiscograficas.setRowCount(0);
                    listarTablas(Tipo.DISCOGRAFICA);
                    anadirCombos();
                    Util.MensajeDeInformacion("Discográfica eliminada correctamente");


                }

                break;

            case DISCO:

                if (modificado == true){

                    String artistaDelDisco = cbDiscoArtista.getSelectedItem().toString();
                    String discograficaDelDisco = cbDiscoCompania.getSelectedItem().toString();

                        sql = "UPDATE Discos SET titulo = ?, artista = ?, copias_vendidas = ?, precio_salida = ?, compania_discografica = ?, fecha_salida = ? WHERE titulo = ? AND artista = ?";

                        try {
                            PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                            sentencia.setString(1, tfDiscoTitulo.getText());
                            sentencia.setString(2, artistaDelDisco);
                            sentencia.setInt(3, Integer.parseInt(tfDiscoCopiasVendidas.getText()));
                            sentencia.setFloat(4, Float.parseFloat(tfDiscoPrecioSalida.getText()));
                            sentencia.setString(5, discograficaDelDisco);
                            sentencia.setDate(6, new Date(calDiscoFechaSalida.getDate().getTime()));
                            sentencia.setString(7, discoNombre);
                            sentencia.setString(8, discoArtista);

                            sentencia.executeUpdate();
                        } catch (SQLException sqle) {
                            sqle.printStackTrace();
                            Util.MensajeDeAlerta("Error al dar de alta");
                        }

                } else {

                    try {

                        sql = "DELETE FROM Discos WHERE titulo = ? and artista = ?;";

                        PreparedStatement sentencia = conexionBase.prepareStatement(sql);
                        sentencia.setString(1, tfDiscoTitulo.getText());
                        sentencia.setString(2, cbDiscoArtista.getSelectedItem().toString());

                        sentencia.executeUpdate();

                    } catch (SQLException sqle) {
                        Util.MensajeDeAlerta("Error al eliminar");
                    }
                    activarCajasTrasEliminar(Tipo.DISCO, false);
                    mtDiscos.setRowCount(0);
                    listarTablas(Tipo.DISCO);
                    anadirCombos();
                    Util.MensajeDeInformacion("Disco eliminado correctamente");

                }

                break;
        }

    }

    private void activarCajasTrasEliminar(Tipo tipo, boolean nuevo){

            switch (tipo){

                case ARTISTA:
                    tfArtistaNombre.setText("");
                    tfArtistaNombreOriginal.setText("");
                    tfArtistaNumeroTlfno.setText("");
                    tfArtistaPrecioActuacion.setText("");
                    tfArtistaCiudadNacimiento.setText("");
                    calArtistaFechaNacimiento.setDate(null);

                    artistaModificado = false;

                    tfArtistaNombre.setEnabled(nuevo);
                    tfArtistaNombreOriginal.setEnabled(nuevo);
                    tfArtistaNumeroTlfno.setEnabled(nuevo);
                    tfArtistaPrecioActuacion.setEnabled(nuevo);
                    tfArtistaCiudadNacimiento.setEnabled(nuevo);
                    calArtistaFechaNacimiento.setEnabled(nuevo);
                    btArtistaNuevo.setEnabled(!nuevo);
                    btArtistaModificar.setEnabled(nuevo);
                    btArtistaEliminar.setEnabled(nuevo);
                    btArtistaGuardar.setEnabled(nuevo);

                    break;

                case DISCOGRAFICA:

                    tfDiscograficaNombre.setText("");
                    tfDiscograficaNumeroArtistas.setText("");
                    tfDiscograficaCiudadSede.setText("");
                    tfDiscograficaDineroRecaudado.setText("");
                    rbDiscograficaSiTributa.setSelected(false);
                    rbDiscograficaNoTributa.setSelected(false);
                    calDiscograficaFechaRegistro.setDate(null);

                    discograficaModificada = false;

                    tfDiscograficaNombre.setEnabled(nuevo);
                    tfDiscograficaNumeroArtistas.setEnabled(nuevo);
                    tfDiscograficaCiudadSede.setEnabled(nuevo);
                    tfDiscograficaDineroRecaudado.setEnabled(nuevo);
                    rbDiscograficaSiTributa.setEnabled(nuevo);
                    rbDiscograficaNoTributa.setEnabled(nuevo);
                    cbDiscograficaArtistaEstrella.setEnabled(nuevo);
                    calDiscograficaFechaRegistro.setEnabled(nuevo);

                    btDiscograficaNuevo.setEnabled(!nuevo);
                    btDiscograficaModificar.setEnabled(nuevo);
                    btDiscograficaEliminar.setEnabled(nuevo);
                    btDiscograficaGuardar.setEnabled(nuevo);

                    break;

                case DISCO:

                    tfDiscoTitulo.setText("");
                    tfDiscoCopiasVendidas.setText("");
                    tfDiscoPrecioSalida.setText("");
                    calDiscoFechaSalida.setDate(null);

                    discoModificado = false;

                    tfDiscoTitulo.setEnabled(nuevo);
                    cbDiscoArtista.setEnabled(nuevo);
                    tfDiscoCopiasVendidas.setEnabled(nuevo);
                    tfDiscoPrecioSalida.setEnabled(nuevo);
                    cbDiscoCompania.setEnabled(nuevo);
                    calDiscoFechaSalida.setEnabled(nuevo);

                    btDiscoNuevo.setEnabled(!nuevo);
                    btDiscoModificar.setEnabled(nuevo);
                    btDiscoEliminar.setEnabled(nuevo);
                    btDiscoGuardar.setEnabled(nuevo);

                    break;

            }
        }

    private void buscar(Tipo tipo, String Filtro) {

        switch (tipo){

            case ARTISTA:

                Filtro = "\""+Filtro+"%\"";

                if (Filtro.length() <= 4){
                    mtArtistas.setRowCount(0);
                    listarTablas(Tipo.ARTISTA);
                } else {

                    try {

                        Statement stmt = conexionBase.createStatement();
                        ResultSet rs;

                        rs = stmt.executeQuery("SELECT * FROM Artistas WHERE nombre_artistico LIKE "+Filtro+";");
                        mtArtistas.setRowCount(0);

                        while ( rs.next() ) {
                            String nombre_artistico = rs.getString("nombre_artistico");
                            String nombre_original = rs.getString("numero_telefono");
                            int numero_telefono = rs.getInt("numero_telefono");
                            Float precio_actuacion = rs.getFloat("precio_actuacion");
                            String ciudad_nacimiento = rs.getString("ciudad_nacimiento");
                            Date fecha_nacimiento = rs.getDate("fecha_nacimiento");
                            Object[] fila = new Object[]{nombre_artistico, nombre_original, numero_telefono, precio_actuacion, ciudad_nacimiento, fecha_nacimiento};

                            mtArtistas.addRow(fila);
                        }

                    } catch (SQLException sqle) {
                        sqle.printStackTrace();
                    }
                }

                break;

            case DISCOGRAFICA:

                Filtro = "\""+Filtro+"%\"";

                if (Filtro.length() <= 4){
                    mtDiscograficas.setRowCount(0);
                    listarTablas(Tipo.DISCOGRAFICA);
                } else {

                    try {

                        Statement stmt = conexionBase.createStatement();
                        ResultSet rs;

                        rs = stmt.executeQuery("SELECT * FROM Discograficas WHERE nombre_registrado LIKE "+Filtro+";");
                        mtDiscograficas.setRowCount(0);

                        while ( rs.next() ) {
                            int id = rs.getInt("id");
                            String nombre_registrado = rs.getString("nombre_registrado");
                            int numero_artistas_contratados = rs.getInt("numero_artistas_contratados");
                            String ciudad_sede = rs.getString("ciudad_sede");
                            Float dinero_recaudado = rs.getFloat("dinero_recaudado");
                            Boolean tributa = rs.getBoolean("tributa");
                            String artista_estrella = rs.getString("artista_estrella");
                            Date fecha_registro_empresarial = rs.getDate("fecha_registro_empresarial");

                            Object[] fila = new Object[]{nombre_registrado, numero_artistas_contratados, ciudad_sede, dinero_recaudado, tributa, artista_estrella, fecha_registro_empresarial};

                            mtDiscograficas.addRow(fila);
                        }

                    } catch (SQLException sqle) {
                        sqle.printStackTrace();
                    }
                }

                break;

            case DISCO:

                Filtro = "\""+Filtro+"%\"";

                if (Filtro.length() <= 4){
                    mtDiscos.setRowCount(0);
                    listarTablas(Tipo.DISCO);
                } else {

                    try {

                        Statement stmt = conexionBase.createStatement();
                        ResultSet rs;

                        rs = stmt.executeQuery("SELECT * FROM Discos WHERE titulo LIKE "+Filtro+";");
                        mtDiscos.setRowCount(0);

                        while ( rs.next() ) {
                            int id = rs.getInt("id");
                            String titulo = rs.getString("titulo");
                            String artista = rs.getString("artista");
                            int copias_vendidas = rs.getInt("copias_vendidas");
                            Float precio_salida = rs.getFloat("precio_salida");
                            String compania_discografica = rs.getString("compania_discografica");
                            Date fecha_salida = rs.getDate("fecha_salida");

                            Object[] fila = new Object[]{titulo, artista, copias_vendidas, precio_salida, compania_discografica, fecha_salida};

                            mtDiscos.addRow(fila);
                        }

                    } catch (SQLException sqle) {
                        sqle.printStackTrace();
                    }
                }

                break;

        }


    }

    private void loguearUsuario(){
        do{
            AllLogins loginUsuarios = new AllLogins("Ingrese en la aplicación");
            loginUsuarios.usuariosAplicacion();
            usuario = loginUsuarios.getUsuario();
            contrasena = loginUsuarios.getContrasena();

            if (usuario.equals("Moby") && contrasena.equals("admin")){
                Util.MensajeDeInformacionCorrecto("Has ingresado como administrador");
                usuarioIdentificado=true;
                logueadoComoAdmin=true;
            } else if (usuario.equals("Usuario") && contrasena.equals("usuario")){
                Util.MensajeDeInformacionCorrecto("Has ingresado correctamente, "+usuario);
                usuarioIdentificado=true;
                logueadoComoUser=true;
            } else {
                Util.MensajeDeAlerta("Error. El usuario no existe. Compruebe que has introducido los datos correctamente.");
                usuarioIdentificado=false;
            }
        } while (usuarioIdentificado==false);
        establecerPermisos();
    }

    private void cogerPermisos(){
        privilegioInsertar=gestion.getInsertar();
        privilegioModificar=gestion.getModificar();
        privilegioEliminar=gestion.getEliminar();
    }

    private void establecerPermisos(){
        if (logueadoComoUser && !privilegioInsertar){
            btArtistaNuevo.setEnabled(false);
            btArtistaGuardar.setEnabled(false);
            btDiscograficaNuevo.setEnabled(false);
            btDiscograficaGuardar.setEnabled(false);
            btDiscoNuevo.setEnabled(false);
            btDiscoGuardar.setEnabled(false);
        }
        if(logueadoComoUser && privilegioInsertar){
            btArtistaNuevo.setEnabled(true);
            btDiscograficaNuevo.setEnabled(true);
            btDiscoNuevo.setEnabled(true);
        }
        if (logueadoComoUser && privilegioModificar==false){
            btArtistaModificar.setEnabled(false);
            btDiscograficaModificar.setEnabled(false);
            btDiscoModificar.setEnabled(false);
        }
        if (logueadoComoUser && privilegioEliminar==false){
            btArtistaEliminar.setEnabled(false);
            btDiscograficaEliminar.setEnabled(false);
            btDiscoEliminar.setEnabled(false);
        }
        if(logueadoComoAdmin){
            activarCajas(Tipo.ARTISTA, true);
            seleccionarEditable(Tipo.ARTISTA, false);
            btArtistaGuardar.setEnabled(false);
            activarCajas(Tipo.DISCOGRAFICA, true);
            seleccionarEditable(Tipo.DISCOGRAFICA, false);
            btDiscograficaGuardar.setEnabled(false);
            activarCajas(Tipo.DISCO, true);
            seleccionarEditable(Tipo.DISCO, false);
            btDiscoGuardar.setEnabled(false);
        }
    }

    private void actualizarMenuBar(){
        frame.remove(getMenuBar());
        frame.setJMenuBar(getMenuBar());
    }

}
