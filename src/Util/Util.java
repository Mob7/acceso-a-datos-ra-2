package Util;

import javax.swing.*;
import java.io.*;

/**
 * Created by sergio on 13/11/2015.
 */
public class Util {


    //Mensaje para avisos importantes después de realizar una acción
    public static void MensajeDeAlerta(String Mensaje) {
        JOptionPane.showMessageDialog(null, Mensaje);
    }

    //Mensaje para confirmar si está seguro de querer realizar una acción
    public static int MensajeDeConfirmacion(String Mensaje){
        return JOptionPane.showConfirmDialog(null, Mensaje);
    }

    //Mensajes de información relevantes a la hora de realizar operaciones
    public static void MensajeDeInformacion(String Mensaje){
        JOptionPane.showMessageDialog(null, Mensaje, "Información", JOptionPane.ERROR_MESSAGE);
    }

    public static void MensajeDeInformacionCorrecto(String Mensaje){
        JOptionPane.showMessageDialog(null, Mensaje, "Información", JOptionPane.INFORMATION_MESSAGE);
    }


}
