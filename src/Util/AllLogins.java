package Util;

import Gui.JLogin;

import javax.swing.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by sergio on 06/12/2015.
 */
public class AllLogins {

    private String titulo;

    public Connection getConexionBaseDeDatos() {
        return conexionBaseDeDatos;
    }

    private Connection conexionBaseDeDatos;

    public String getUsuario() {
        return usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    private String usuario;
    private String contrasena;

    public AllLogins(String titulo){

        this.titulo = titulo;

    }

    public void phpMyAdmin(){

        JLogin login = new JLogin(titulo);
        login.setVisible(true);

        usuario = login.getUsuario();
        contrasena = login.getContrasena();

        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexionBaseDeDatos = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/discostu", usuario, contrasena);
        } catch (ClassNotFoundException cnfe){
            JOptionPane.showMessageDialog(null,
                    "No se ha podido cargar el driver del SGBD",
                    "Fallo al conectar", JOptionPane.ERROR_MESSAGE);
        } catch (SQLException sqle){
            JOptionPane.showMessageDialog(null,
                    "No se ha podido conectar",
                    "Fallo al conectar", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void usuariosAplicacion() {

        JLogin loginUser = new JLogin(titulo);
        loginUser.setVisible(true);

        usuario = loginUser.getUsuario();
        contrasena = loginUser.getContrasena();

    }

}
